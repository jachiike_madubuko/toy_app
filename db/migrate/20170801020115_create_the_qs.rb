class CreateTheQs < ActiveRecord::Migration[5.1]
  def change
    create_table :the_qs do |t|
      t.text :title
      t.integer :user_id

      t.timestamps
    end
  end
end
