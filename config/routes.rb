Rails.application.routes.draw do
  resources :the_qs
  resources :users
  root 'the_qs#new'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
