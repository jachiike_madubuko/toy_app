json.extract! the_q, :id, :title, :user_id, :created_at, :updated_at
json.url the_q_url(the_q, format: :json)
