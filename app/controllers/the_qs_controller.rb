class TheQsController < ApplicationController
  before_action :set_the_q, only: [:show, :edit, :update, :destroy]

  # GET /the_qs
  # GET /the_qs.json
  def index
    @the_qs = TheQ.all
  end

  # GET /the_qs/1
  # GET /the_qs/1.json
  def show
  end

  # GET /the_qs/new
  def new
    @the_q = TheQ.new
  end

  # GET /the_qs/1/edit
  def edit
  end

  # POST /the_qs
  # POST /the_qs.json
  def create
    @the_q = TheQ.new(the_q_params)

    respond_to do |format|
      if @the_q.save
        format.html { redirect_to @the_q, notice: 'The q was successfully created.' }
        format.json { render :show, status: :created, location: @the_q }
      else
        format.html { render :new }
        format.json { render json: @the_q.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /the_qs/1
  # PATCH/PUT /the_qs/1.json
  def update
    respond_to do |format|
      if @the_q.update(the_q_params)
        format.html { redirect_to @the_q, notice: 'The q was successfully updated.' }
        format.json { render :show, status: :ok, location: @the_q }
      else
        format.html { render :edit }
        format.json { render json: @the_q.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /the_qs/1
  # DELETE /the_qs/1.json
  def destroy
    @the_q.destroy
    respond_to do |format|
      format.html { redirect_to the_qs_url, notice: 'The q was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_the_q
      @the_q = TheQ.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def the_q_params
      params.require(:the_q).permit(:title, :user_id)
    end
end
