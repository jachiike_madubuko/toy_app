require 'test_helper'

class TheQsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @the_q = the_qs(:one)
  end

  test "should get index" do
    get the_qs_url
    assert_response :success
  end

  test "should get new" do
    get new_the_q_url
    assert_response :success
  end

  test "should create the_q" do
    assert_difference('TheQ.count') do
      post the_qs_url, params: { the_q: { title: @the_q.title, user_id: @the_q.user_id } }
    end

    assert_redirected_to the_q_url(TheQ.last)
  end

  test "should show the_q" do
    get the_q_url(@the_q)
    assert_response :success
  end

  test "should get edit" do
    get edit_the_q_url(@the_q)
    assert_response :success
  end

  test "should update the_q" do
    patch the_q_url(@the_q), params: { the_q: { title: @the_q.title, user_id: @the_q.user_id } }
    assert_redirected_to the_q_url(@the_q)
  end

  test "should destroy the_q" do
    assert_difference('TheQ.count', -1) do
      delete the_q_url(@the_q)
    end

    assert_redirected_to the_qs_url
  end
end
